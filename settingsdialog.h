#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QStringListModel>
#include <QListWidgetItem>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = nullptr);
    ~SettingsDialog();

private:
    Ui::SettingsDialog *ui;
    QStringList strList;
    QStringListModel strListModel;

protected:
    void showEvent(QShowEvent *) override;

private slots:
    void accept() override;
};

#endif // SETTINGSDIALOG_H
